package com.dema.timeregistration.service.exception;

public class EntityNotFoundException extends EntityException {
    public EntityNotFoundException(String message) {
        super(message);
    }

    public static EntityNotFoundException userNotFound() {
        return new EntityNotFoundException("The specified user could not be found");
    }

    public static EntityNotFoundException companyNotFound() {
        return new EntityNotFoundException("The specified company could not be found");
    }

    public static EntityNotFoundException projectNotFound() {
        return new EntityNotFoundException("The specified project could not be found");
    }

    public static EntityNotFoundException taskNotFound() {
        return new EntityNotFoundException("The specified task could not be found");
    }
}
