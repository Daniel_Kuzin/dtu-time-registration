package com.dema.timeregistration.service.exception;

public class EntityException extends Exception {
    public EntityException(String message) {
        super(message);
    }
}
