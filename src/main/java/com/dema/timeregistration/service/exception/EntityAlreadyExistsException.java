package com.dema.timeregistration.service.exception;

public class EntityAlreadyExistsException extends EntityException {
    public EntityAlreadyExistsException(String message) {
        super(message);
    }
}
