package com.dema.timeregistration.service;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.entity.*;
import com.dema.timeregistration.repository.ProjectRepository;
import com.dema.timeregistration.service.exception.EntityNotFoundException;

import java.sql.Date;
import java.time.Duration;
import java.util.Set;
import java.util.stream.Collectors;

public class GenerateReportService {
    private final ProjectRepository projectRepository;

    public GenerateReportService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public String GenerateReport(int projectId) throws EntityNotFoundException {
        StringBuilder sb = new StringBuilder();
        Project project = projectRepository.findById(projectId).orElseThrow(EntityNotFoundException::projectNotFound);
        Set<Task> tasks = project.getTasks();
        Set<ProjectRole> projectLeaders = project.getProjectRoles().stream().filter(pr -> pr.getRole().equals(Role.PROJECT_LEADER)).collect(Collectors.toSet());
        Set<ProjectRole> developers = project.getProjectRoles().stream().filter(pr -> pr.getRole().equals(Role.SR_DEVELOPER)).collect(Collectors.toSet());
        Duration totalTimeSpentOnProject = Duration.ofSeconds(0);
        Duration totalProjectEstimate = Duration.ofSeconds(0);


        sb.append("\n\n~~~~~PROJECT REPORT~~~~~\n\n")
                .append("Project: ").append(project.getName())
                .append(", Due Date: ").append(Utils.getDateFormat().format(Date.from(project.getDueDate())))
                .append("\n");

        appendProjectLeaders(sb, projectLeaders);
        appendDevelopers(sb, developers);


        sb.append("TASKS:\n");

        for (Task task : tasks) {
            Set<TimeEntry> timeEntries = task.getTimeEntries();
            Duration totalTimeSpendOnTask = Duration.ofSeconds(0);
            //Should also gather all the estimates of each tasks in a var
            for (TimeEntry timeEntry : timeEntries) {
                //Something doesnt work here
                Duration timeElapsed = Duration.between(timeEntry.getStartTime(), timeEntry.getEndTime());
                totalTimeSpendOnTask = totalTimeSpendOnTask.plus(timeElapsed);
            }

            sb.append(task.getName())
                    .append(" - Status: ").append(task.getStatus())
                    .append(", Estimate: ").append(task.getEstimate())
                    .append(", Time Spent: ").append(formatDuration(totalTimeSpendOnTask))
                    .append("\n");

            totalProjectEstimate = totalProjectEstimate.plus(task.getEstimate());
            totalTimeSpentOnProject = totalTimeSpentOnProject.plus(totalTimeSpendOnTask);
        }

        if (tasks.isEmpty()) {
            sb.append("There is currently no tasks for this project\n");
        }

        sb.append("\n  - TOTAL TIME SPENT ON PROJECT:\n    ").append(formatDuration(totalTimeSpentOnProject)).append("\n");
        sb.append("  - TOTAL PROJECT ESTIMATE:\n    ").append(formatDuration(totalProjectEstimate)).append("\n");

        return sb.toString();
    }

    private void appendDevelopers(StringBuilder sb, Set<ProjectRole> developers) {
        sb.append("DEVELOPERS:\n");
        for (ProjectRole developer : developers) {

            if (developer.getRole().equals(Role.SR_DEVELOPER)) {
                sb.append(developer.getUser().getName()).append("\n");
            }
        }

        if (developers.isEmpty()) {
            sb.append("There are currently no developers assigned to this project\n");
        }
    }

    private void appendProjectLeaders(StringBuilder sb, Set<ProjectRole> projectLeaderRoles) {
        sb.append("PROJECT LEADERS:\n");

        for (ProjectRole projectLeader : projectLeaderRoles) {
            if (projectLeader.getRole().equals(Role.PROJECT_LEADER)) {
                sb.append(projectLeader.getUser().getName()).append(" (").append(projectLeader.getUser().getEmail()).append(")").append("\n");
            }
        }
    }

    private String formatDuration(Duration duration) {
        long seconds = duration.getSeconds();
        long absSeconds = Math.abs(seconds);
        return String.format(
                "%d:%02d:%02d",
                absSeconds / 3600,
                (absSeconds % 3600) / 60,
                absSeconds % 60);
    }
}
