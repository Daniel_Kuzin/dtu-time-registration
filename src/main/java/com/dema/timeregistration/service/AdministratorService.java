package com.dema.timeregistration.service;

import com.dema.timeregistration.entity.Company;
import com.dema.timeregistration.entity.GlobalRole;
import com.dema.timeregistration.entity.User;
import com.dema.timeregistration.repository.CompanyRepository;
import com.dema.timeregistration.repository.UserRepository;
import com.dema.timeregistration.service.exception.AccessDeniedException;
import com.dema.timeregistration.service.exception.EntityAlreadyExistsException;
import com.dema.timeregistration.service.exception.EntityNotFoundException;
import com.dema.timeregistration.service.models.UpdateCompanyRequest;

import java.util.stream.Collectors;

public class AdministratorService {
    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;
    private final UserAccessService userAccessService;

    public AdministratorService(UserRepository userRepository,
                                CompanyRepository companyRepository,
                                UserAccessService userAccessService) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.userAccessService = userAccessService;
    }

    public void setGlobalRole(int userId, GlobalRole globalRole, UserContext userContext) throws EntityNotFoundException, AccessDeniedException {
        userAccessService.throwIfUserIsNotAdmin(userContext);

        if (globalRole != GlobalRole.GLOBAL_ADMINISTRATOR && userRepository.countByGlobalRole(GlobalRole.GLOBAL_ADMINISTRATOR) == 1) {
            throw new AccessDeniedException("There must be at least one administrator at all times");
        }

        User user = userRepository.findById(userId).orElseThrow(EntityNotFoundException::userNotFound);
        user.setGlobalRole(globalRole);
        userRepository.save(user);
    }

    public void createCompany(String name, UserContext userContext) throws AccessDeniedException, EntityNotFoundException, EntityAlreadyExistsException {
        userAccessService.throwIfUserIsNotAdmin(userContext);

        if (companyRepository.existsByName(name)) {
            throw new EntityAlreadyExistsException("A company with the specified name already exists");
        }

        Company company = Company.create(name);
        companyRepository.save(company);
    }

    public void updateCompany(UpdateCompanyRequest updateCompanyRequest, UserContext userContext) throws EntityNotFoundException, AccessDeniedException, EntityAlreadyExistsException {
        userAccessService.throwIfUserIsNotAdmin(userContext);

        if (companyRepository.existsByNameAndIdNot(updateCompanyRequest.getName(), updateCompanyRequest.getId())) {
            throw new EntityAlreadyExistsException("A company with the specified name already exists");
        }

        Company company = companyRepository.findById(updateCompanyRequest.getId()).orElseThrow(EntityNotFoundException::companyNotFound);

        if (updateCompanyRequest.getName() != null) {
            company.setName(updateCompanyRequest.getName());
        }

        companyRepository.save(company);
    }

    public int getCompanyId(String name) throws EntityNotFoundException {
        return this.companyRepository.findByName(name).orElseThrow(EntityNotFoundException::companyNotFound).getId();
    }

    public String listCompanies() {
        return companyRepository.findAll().stream()
                .map(Company::getName)
                .collect(Collectors.joining("\n"));
    }
}
