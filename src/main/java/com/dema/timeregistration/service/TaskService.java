package com.dema.timeregistration.service;

import com.dema.timeregistration.entity.Company;
import com.dema.timeregistration.entity.Project;
import com.dema.timeregistration.entity.Task;
import com.dema.timeregistration.repository.CompanyRepository;
import com.dema.timeregistration.repository.ProjectRepository;
import com.dema.timeregistration.repository.TaskRepository;
import com.dema.timeregistration.service.exception.AccessDeniedException;
import com.dema.timeregistration.service.exception.EntityAlreadyExistsException;
import com.dema.timeregistration.service.exception.EntityNotFoundException;
import com.dema.timeregistration.service.models.CreateTaskRequest;
import com.dema.timeregistration.service.models.UpdateTaskRequest;

import java.time.Duration;
import java.util.stream.Collectors;

public class TaskService {
    private final TaskRepository taskRepository;
    private final ProjectRepository projectRepository;
    private final CompanyRepository companyRepository;
    private final UserAccessService userAccessService;

    public TaskService(TaskRepository taskRepository, ProjectRepository projectRepository, CompanyRepository companyRepository, UserAccessService userAccessService) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.companyRepository = companyRepository;
        this.userAccessService = userAccessService;
    }

    public void createTask(CreateTaskRequest request, UserContext userContext) throws EntityNotFoundException, AccessDeniedException, EntityAlreadyExistsException {
        Project project = projectRepository.findById(request.getProjectId()).orElseThrow(EntityNotFoundException::projectNotFound);
        this.userAccessService.throwIfUserIsNotProjectLeaderOrAdmin(project, userContext);

        if (taskRepository.existsByProjectAndName(project, request.getName())) {
            throw new EntityAlreadyExistsException("A task with the name '" + request.getName() + "' already exists");
        }

        Duration estimate = Duration.parse(request.getEstimate());

        Task task = Task.create(
                request.getName(),
                project,
                request.getDescription(),
                request.getStatus(),
                estimate
        );

        taskRepository.save(task);
    }

    public void updateTask(UpdateTaskRequest request, UserContext userContext) throws EntityNotFoundException, AccessDeniedException, EntityAlreadyExistsException {
        Task task = taskRepository.findById(request.getId()).orElseThrow(EntityNotFoundException::taskNotFound);
        this.userAccessService.throwIfUserIsNotProjectLeaderOrAdmin(task.getProject(), userContext);

        if (taskRepository.existsByProjectAndNameAndIdNot(task.getProject(), request.getName(), task.getId())) {
            throw new EntityAlreadyExistsException("A task with the name '" + request.getName() + "' already exists");
        }

        if (request.getName() != null) {
            task.setName(request.getName());
        }

        if (request.getDescription() != null) {
            task.setDescription(request.getDescription());
        }

        if (request.getEstimate() != null) {
            task.setEstimate(Duration.parse(request.getEstimate()));
        }

        if (request.getStatus() != null) {
            task.setStatus(request.getStatus());
        }

        taskRepository.save(task);
    }

    public int getTaskId(String company, String project, String task) throws EntityNotFoundException {
        Company foundCompany = companyRepository.findByName(company).orElseThrow(EntityNotFoundException::companyNotFound);
        Project foundProject = projectRepository.findByCompanyAndName(foundCompany, project).orElseThrow(EntityNotFoundException::projectNotFound);
        Task foundTask = taskRepository.findByProjectAndName(foundProject, task).orElseThrow(EntityNotFoundException::taskNotFound);

        return foundTask.getId();
    }

    public String listTasks(int projectId) throws EntityNotFoundException {
        Project project = projectRepository.findById(projectId).orElseThrow(EntityNotFoundException::projectNotFound);

        return project.getTasks().stream()
                .map(Task::getName)
                .collect(Collectors.joining("\n"));
    }
}
