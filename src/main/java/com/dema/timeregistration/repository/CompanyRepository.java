package com.dema.timeregistration.repository;

import com.dema.timeregistration.entity.Company;

import java.util.List;
import java.util.Optional;

public interface CompanyRepository extends CrudRepository<Company> {
    Optional<Company> findByName(String name);

    boolean existsByName(String name);

    boolean existsByNameAndIdNot(String name, int id);

    List<Company> findAll();
}
