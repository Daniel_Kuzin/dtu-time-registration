package com.dema.timeregistration.repository.jpa;

import com.dema.timeregistration.entity.EntityWithId;
import com.dema.timeregistration.repository.CrudRepository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.ParameterExpression;
import javax.persistence.criteria.Root;
import java.util.Optional;

/**
 * Extend this class to create a Jpa based repository with basic CRUD functionality.
 *
 * @param <TEntity> The entity type that the repository will provide access to
 */
public abstract class AbstractJpaRepository<TEntity extends EntityWithId> implements CrudRepository<TEntity> {
    protected final EntityManager entityManager;

    /**
     * We can't obtain a reference to the Class type
     */
    private final Class<TEntity> type;

    public AbstractJpaRepository(EntityManager entityManager, Class<TEntity> type) {
        this.entityManager = entityManager;
        this.type = type;
    }


    @Override
    public void save(TEntity entity) {
        withinTransaction(() -> {
            if (entity.getId() == 0) {
                // If an entity has Id 0, we assume that it has never been persisted
                entityManager.persist(entity);
            } else {
                entityManager.merge(entity);
            }
        });
    }

    public Optional<TEntity> findById(int id) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<TEntity> cq = cb.createQuery(type);

        Root<TEntity> e = cq.from(type);
        ParameterExpression<Integer> p = cb.parameter(Integer.class);
        cq.select(e).where(cb.equal(e.get("id"), p));

        TypedQuery<TEntity> query = entityManager.createQuery(cq);
        query.setParameter(p, id);

        return query.getResultStream().findFirst();
    }

    @Override
    public void delete(TEntity entity) {
        entityManager.remove(entity);
    }

    protected void withinTransaction(Runnable f) {
        entityManager.getTransaction().begin();
        f.run();
        entityManager.getTransaction().commit();
    }
}
