package com.dema.timeregistration.repository.jpa;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class JpaRepositoryConfiguration {
    public static final Map<String, String> properties;

    static {
        Map<String, String> props = new HashMap<>();
        props.put("javax.persistence.jdbc.url", "jdbc:h2:./db/main");
        properties = Collections.unmodifiableMap(props);
    }
}
