package com.dema.timeregistration.repository;

import com.dema.timeregistration.entity.Project;
import com.dema.timeregistration.entity.Task;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends CrudRepository<Task> {
    Optional<Task> findByProjectAndName(Project project, String name);

    boolean existsByProjectAndName(Project project, String name);

    boolean existsByProjectAndNameAndIdNot(Project project, String name, int id);
}
