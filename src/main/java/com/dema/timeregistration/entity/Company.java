package com.dema.timeregistration.entity;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Company extends EntityWithId {
    @Column(unique = true)
    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "company")
    private Set<Project> projects;

    protected Company() {
    }

    public static Company create(String name) {
        Company company = new Company();
        company.setName(name);
        return company;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Project> getProjects() {
        return projects;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }
}
