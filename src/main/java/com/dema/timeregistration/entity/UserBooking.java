package com.dema.timeregistration.entity;

import com.dema.timeregistration.repository.jpa.InstantAttributeConverter;

import javax.persistence.Convert;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.time.Instant;

@MappedSuperclass
public abstract class UserBooking extends EntityWithId {
    @ManyToOne
    private User user;

    @Convert(converter = InstantAttributeConverter.class)
    private Instant startTime;

    @Convert(converter = InstantAttributeConverter.class)
    private Instant endTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }
}
