package com.dema.timeregistration.entity;

public enum Role {
    PROJECT_LEADER("Project Leader"),
    SR_DEVELOPER("Sr. Developer");

    private final String name;

    Role(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
