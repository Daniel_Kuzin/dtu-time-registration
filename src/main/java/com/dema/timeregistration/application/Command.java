package com.dema.timeregistration.application;

import java.util.Set;

public interface Command {
    String getKeyword();

    Set<Parameter> getParameters();

    void doCommand(ParsedCommand parsedCommand) throws Exception;

    default String getDocumentation() {
        return "This command has no documentation";
    }

    default CommandGroup getCommandGroup() {
        return CommandGroup.NONE;
    }
}
