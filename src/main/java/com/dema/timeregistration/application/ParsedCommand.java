package com.dema.timeregistration.application;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Represents a parsed command with values for each of it's parameters
 */
public class ParsedCommand {
    private final Command command;
    private final Map<Parameter, String> values = new HashMap<>();

    public ParsedCommand(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }

    public void addParsedParameter(Parameter parameter, String value) {
        values.put(parameter, value);
    }

    public Optional<String> getOptional(String name) {
        Optional<Parameter> param = values
                .keySet()
                .stream()
                .filter(p -> p.getName().equals(name))
                .findFirst();

        return param.map(values::get);
    }

    public String get(String name) {
        Parameter parameter = values
                .keySet()
                .stream()
                .filter(p -> p.getName().equals(name))
                .findFirst().orElseThrow(() -> new IllegalStateException("Parsed command does not contain required parameter with keyword " + name));

        return values.get(parameter);
    }

    public Set<Parameter> getMissingParamters() {
        return command
                .getParameters()
                .stream()
                .filter(Parameter::isRequired)
                .filter(p -> !values.containsKey(p))
                .collect(Collectors.toSet());
    }
}
