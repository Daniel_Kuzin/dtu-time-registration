package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.*;
import com.dema.timeregistration.service.TaskService;
import com.dema.timeregistration.service.TimeEntryService;
import com.dema.timeregistration.service.models.UpdateTimeEntryRequest;

import java.util.Set;

public class UpdateTimeEntryCommand implements Command {
    private final TimeEntryService timeEntryService;
    private final TaskService taskService;
    private final ApplicationContext applicationContext;

    public UpdateTimeEntryCommand(TimeEntryService timeEntryService, TaskService taskService, ApplicationContext applicationContext) {
        this.timeEntryService = timeEntryService;
        this.taskService = taskService;
        this.applicationContext = applicationContext;
    }

    @Override
    public String getDocumentation() {
        return "Change the periode of a time entry for a given task using parameters:\n-o the original start time of the" +
                " time entry\n-c name of company\n-p name of project\n-t name of task\n-s new start time\n-e new end time\nExample of usage:\n" +
                "update-time -o \"01/01/2019 12:00\" -c \"My Company\" -p \"My Project\" -t \"My Task\" -s \"01/01/2019 12:00\" -e \"30/12/2019 12:00\"";
    }

    @Override
    public String getKeyword() {
        return "update-time";
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("o"),
                new Parameter("c"),
                new Parameter("p"),
                new Parameter("t"),
                new Parameter("s"),
                new Parameter("e")
        );
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        int taskId = taskService.getTaskId(parsedCommand.get("c"), parsedCommand.get("p"), parsedCommand.get("t"));

        UpdateTimeEntryRequest request = new UpdateTimeEntryRequest();
        request.setTaskId(taskId);
        request.setOriginalStartTime(parsedCommand.get("o"));

        parsedCommand.getOptional("s").ifPresent(request::setStartTime);
        parsedCommand.getOptional("e").ifPresent(request::setEndTime);

        timeEntryService.updateTimeEntry(request, applicationContext.getUserContextOrThrow());  
        System.out.println("Success");
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.TIME_ENTRY;
    }
}
