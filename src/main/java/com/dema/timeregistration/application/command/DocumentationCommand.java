package com.dema.timeregistration.application.command;

import com.dema.timeregistration.application.Command;
import com.dema.timeregistration.application.CommandInterpreter;
import com.dema.timeregistration.application.Parameter;
import com.dema.timeregistration.application.ParsedCommand;

import java.util.HashSet;
import java.util.Set;

public class DocumentationCommand implements Command {
    private CommandInterpreter commandInterpreter;

    public void setCommandInterpreter(CommandInterpreter commandInterpreter) {
        this.commandInterpreter = commandInterpreter;
    }

    @Override
    public String getKeyword() {
        return "help";
    }

    @Override
    public Set<Parameter> getParameters() {
        return new HashSet<>();
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) {
        System.out.println(commandInterpreter.generateDocumentation());
    }

    @Override
    public String getDocumentation() {
        return "Get a list of commands";
    }
}
