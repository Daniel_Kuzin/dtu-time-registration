package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.*;
import com.dema.timeregistration.service.UserBookingService;
import com.dema.timeregistration.service.models.RegisterUserAbsenceRequest;

import java.util.Set;

public class RegisterUserAbcenseCommand implements Command {
    private final UserBookingService userBookingService;
    private final ApplicationContext applicationContext;

    public RegisterUserAbcenseCommand(UserBookingService userBookingService, ApplicationContext applicationContext) {
        this.userBookingService = userBookingService;
        this.applicationContext = applicationContext;
    }

    @Override
    public String getDocumentation() {
        return "Register your absence as a user with following parameters:\n-s start time of absence\n-e end time of" +
                " absence\n-r reason for absence\nExample of usage:\nregister-absence -s \"01/01/2019 12:00\" -e \"01/02/2019 12:00\" -r \"Vacation\"";
    }

    @Override
    public String getKeyword() {
        return "register-absence";
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("s"),
                new Parameter("e"),
                new Parameter("r")
        );
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.USER_BOOKING;
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        RegisterUserAbsenceRequest request = new RegisterUserAbsenceRequest();
        request.setStartTime(parsedCommand.get("s"));
        request.setEndTime(parsedCommand.get("e"));
        request.setReason(parsedCommand.get("r"));

        userBookingService.registerUserAbsence(request, applicationContext.getUserContextOrThrow());
        System.out.println("Success");
    }
}
