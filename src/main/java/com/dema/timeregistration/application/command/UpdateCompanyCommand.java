package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.*;
import com.dema.timeregistration.service.AdministratorService;
import com.dema.timeregistration.service.models.UpdateCompanyRequest;

import java.util.Set;

public class UpdateCompanyCommand implements Command {
    private final AdministratorService administratorService;
    private final ApplicationContext applicationContext;

    public UpdateCompanyCommand(AdministratorService administratorService, ApplicationContext applicationContext) {
        this.administratorService = administratorService;
        this.applicationContext = applicationContext;
    }

    @Override
    public String getDocumentation() {
        return "Change the name of a company. The command has following parameters:\n-o for old company name\n-n" +
                " for new company name\nExample of usage:\nupdate-company -o \"Old Company Name\" -n \"New Company Name\"";
    }

    @Override
    public String getKeyword() {
        return "update-company";
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.ADMIN;
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("o"),
                new Parameter("n", false)
        );
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        UpdateCompanyRequest request = new UpdateCompanyRequest();

        request.setId(administratorService.getCompanyId(parsedCommand.get("o")));
        parsedCommand.getOptional("n").ifPresent(request::setName);
        administratorService.updateCompany(request, applicationContext.getUserContextOrThrow());

        System.out.println("The company was updated successfully");
    }
}
