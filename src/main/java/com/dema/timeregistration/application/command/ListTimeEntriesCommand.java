package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.*;
import com.dema.timeregistration.service.TimeEntryService;

import java.util.Set;

public class ListTimeEntriesCommand implements Command {
    private final TimeEntryService timeEntryService;
    private final ApplicationContext applicationContext;

    public ListTimeEntriesCommand(TimeEntryService timeEntryService, ApplicationContext applicationContext) {
        this.timeEntryService = timeEntryService;
        this.applicationContext = applicationContext;
    }

    @Override
    public String getKeyword() {
        return "list-time-entries";
    }

    @Override
    public String getDocumentation() {
        return "Get a list of time entries for a given task using parameters:\n-f from a given time\n -t to a given time" +
                "\nExample of usage:\nlist-time-entries -f \"01/01/2019 12:00\" -t \"31/12/2019 12:00\"";
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("f"),
                new Parameter("t")
        );
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.GENERAL;
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        if(timeEntryService.listTimeEntries(
                parsedCommand.get("f"),
                parsedCommand.get("t"),
                applicationContext.getUserContextOrThrow()).isEmpty()) {
            System.out.println("No time entries found with the given parameters.");
        } else {
            System.out.println(timeEntryService.listTimeEntries(
                    parsedCommand.get("f"),
                    parsedCommand.get("t"),
                    applicationContext.getUserContextOrThrow()));
        }
    }
}
