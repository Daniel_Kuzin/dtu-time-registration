package com.dema.timeregistration.application.command;

import com.dema.timeregistration.Utils;
import com.dema.timeregistration.application.*;
import com.dema.timeregistration.service.ProjectService;
import com.dema.timeregistration.service.UserBookingService;
import com.dema.timeregistration.service.UserService;
import com.dema.timeregistration.service.models.BookUserRequest;

import java.util.Set;

public class BookUserCommand implements Command {
    private final UserBookingService userBookingService;
    private final UserService userService;
    private final ProjectService projectService;
    private final ApplicationContext applicationContext;

    public BookUserCommand(UserBookingService userBookingService, UserService userService, ProjectService projectService, ApplicationContext applicationContext) {
        this.userBookingService = userBookingService;
        this.userService = userService;
        this.projectService = projectService;
        this.applicationContext = applicationContext;
    }

    @Override
    public String getDocumentation() {
        return "Book a user for a project using parameters:\n-u email of the user\n-p name of project\n-c name of" +
                " company\n-s start time of booking\n-e end time of booking\nExample of usage:\nbook-user -u" +
                " \"daniel@kuzin.dk\" -p \"My Project\" -c \"My Company\" -s \"01/01/2019 12:00\" -e \"31/01/2019 12:00\"";
    }

    @Override
    public CommandGroup getCommandGroup() {
        return CommandGroup.USER_BOOKING;
    }

    @Override
    public String getKeyword() {
        return "book-user";
    }

    @Override
    public Set<Parameter> getParameters() {
        return Utils.newHashSet(
                new Parameter("u"),
                new Parameter("p"),
                new Parameter("c"),
                new Parameter("s"),
                new Parameter("e")
        );
    }

    @Override
    public void doCommand(ParsedCommand parsedCommand) throws Exception {
        int userId = userService.getUserId(parsedCommand.get("u"));

        int projectId = projectService.getProjectId(parsedCommand.get("c"), parsedCommand.get("p"));

        BookUserRequest bookUserRequest = new BookUserRequest();
        bookUserRequest.setUserId(userId);
        bookUserRequest.setProjectId(projectId);
        bookUserRequest.setStartTime(parsedCommand.get("s"));
        bookUserRequest.setEndTime(parsedCommand.get("e"));

        userBookingService.bookUser(bookUserRequest, applicationContext.getUserContextOrThrow());
        System.out.println("The user was booked succesfully");
    }
}
