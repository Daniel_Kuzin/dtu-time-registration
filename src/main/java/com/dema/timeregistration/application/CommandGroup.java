package com.dema.timeregistration.application;

public enum CommandGroup {
    NONE("None"),
    GENERAL("General Actions"),
    ADMIN("Global Administration"),
    ACCOUNT("Account Actions"),
    PROJECT_ADMINISTRATION("Project Administration"),
    TIME_ENTRY("Time Entry"),
    USER_BOOKING("User Booking");

    private String name;

    CommandGroup(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
