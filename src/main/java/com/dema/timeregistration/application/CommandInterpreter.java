package com.dema.timeregistration.application;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class CommandInterpreter {
    private static final Pattern pattern = Pattern.compile("\"[^\"]+\"|\\S+");

    /**
     * Commands a referred to by their keywords, which are the key of this Map.
     */
    private final Map<String, Command> commands;

    public CommandInterpreter(Set<Command> commands) {
        this.commands = commands.stream().collect(Collectors.toMap(Command::getKeyword, c -> c));
    }

    /**
     * Reads and formats documentation for every registered command. Commands are divided into Command Groups to make it
     * easier to find commands in the documentation. Documentation can be added in the getDocumentation() function in
     * a command implementing the Command interface. The command can be given a command group with the getCommandGroup()
     * function.
     */
    public String generateDocumentation() {
        StringBuilder sb = new StringBuilder();

        sb.append("Commands:\n\n");

        Map<String, List<Command>> commandGroups = commands.values()
                .stream()
                .collect(Collectors.groupingBy(c -> c.getCommandGroup().getName()));

        commandGroups.values().forEach(l -> l.sort(Comparator.comparing(Command::getKeyword)));

        for (Map.Entry<String, List<Command>> commandGroup : commandGroups.entrySet()) {
            sb.append("Command Group '").append(commandGroup.getKey()).append("':\n");

            for (Command c : commandGroup.getValue()) {
                sb.append(c.getKeyword()).append(" (Parameters: ");
                sb.append(c.getParameters().stream().map(p -> "-" + p.getName() + (!p.isRequired() ? " (Optional)" : "")).collect(Collectors.joining(", ")));

                if (c.getParameters().isEmpty()) {
                    sb.append("None");
                }

                sb.append(")\n");

                sb.append(c.getDocumentation()).append("\n\n");
            }

            sb.append("\n");
        }

        return sb.toString();
    }

    /**
     * Splits a string in to words or quoted strings. For example:
     * register -e mads@kahler.biz -n "Mads Kahler"
     * is divided into the following parts:
     * register
     * -e
     * mads@kahler.biz
     * -n
     * "Mads Kahler"
     */
    private List<String> getParts(String commandString) {
        Matcher matcher = pattern.matcher(commandString);

        List<String> parts = new ArrayList<>();

        while (matcher.find()) {
            parts.add(matcher.group());
        }

        return parts;
    }

    /**
     * Parses the command keyword and parameters for a given string
     */
    public ParsedCommand parseCommand(String commandString) throws UnrecognizedCommandException {
        List<String> parts = getParts(commandString);

        if (parts.isEmpty()) {
            throw new UnrecognizedCommandException("No command was entered");
        }

        String keyword = parts.get(0);

        if (!commands.containsKey(keyword)) {
            throw new UnrecognizedCommandException("No command with the keyword '" + keyword + "' was found");
        }

        Command command = commands.get(keyword);
        Map<String, Parameter> parameters = command.getParameters().stream().collect(Collectors.toMap(Parameter::getName, p -> p));
        ParsedCommand parsedCommand = new ParsedCommand(command);

        for (int i = 1; i < parts.size(); i += 2) {
            String parameterName = parts.get(i);

            if (!parameterName.startsWith("-")) {
                throw new UnrecognizedCommandException("Expected parameter to start with a dash (-)");
            }

            parameterName = parameterName.substring(1); // Remove the dash

            if (!parameters.containsKey(parameterName)) {
                throw new UnrecognizedCommandException("The command with keyword '" + command.getKeyword() +
                        "' does not contain a definition for the parameter '" + parameterName + "'");
            }

            Parameter parameter = parameters.get(parameterName);

            if (parts.size() < i + 2) {
                throw new UnrecognizedCommandException("Expected value for the parameter '" + parameterName + "'");
            }

            String value = parts.get(i + 1);

            if (value.startsWith("-")) {
                throw new UnrecognizedCommandException("Unexpected parameter '" + value +
                        "' provided as value for parameter '" + parameterName + "'");
            }

            // Remove surrounding quotation marks if needed
            if (value.startsWith("\"") && value.endsWith("\"")) {
                value = value.substring(1, value.length() - 1);
            }

            parsedCommand.addParsedParameter(parameter, value);
        }

        Set<Parameter> missingParameters = parsedCommand.getMissingParamters();

        if (!missingParameters.isEmpty()) {
            throw new UnrecognizedCommandException("Missing parameters: " +
                    String.join(", ", missingParameters
                            .stream()
                            .map(p -> "-" + p.getName())
                            .collect(Collectors.toSet())));
        }

        return parsedCommand;
    }
}
