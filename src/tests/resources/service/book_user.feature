Feature: User with project role tries to book user
  Description: A user with a project role books a user so that they may register their time on a project
  Actors: User with project role

  Scenario: User is booked successfully
    Given a company with the the following
      | Name | DTU |
    # Projects must have an initial project leader, giving this role to mads@kahler.biz is a quick workaround for this test
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Test Project    |
      | ProjectLeader | mads@kahler.biz |
    And the actor is a project leader for project "Test Project" for company "DTU"
    And the subject is a user
    When the actor books the subject to the project "Test Project" for company "DTU" from "13/05/2019 12:00" to "13/05/2019 16:00"
    Then the subject should have a booking to the project "Test Project" for company "DTU" from "13/05/2019 12:00" to "13/05/2019 16:00"

  Scenario: User is not available in period due to absence
    Given a company with the the following
      | Name | DTU |
    # Projects must have an initial project leader, giving this role to mads@kahler.biz is a quick workaround for this test
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Test Project    |
      | ProjectLeader | mads@kahler.biz |
    And the actor is a project leader for project "Test Project" for company "DTU"
    And the subject is a user
    And the subject has an absence with the following
      | Start Time | 10/05/2019 12:50 |
      | End Time   | 10/05/2019 12:55 |
      | Reason     | Sick             |
    When the actor books the subject to the project "Test Project" for company "DTU" from "10/05/2019 10:00" to "10/05/2019 16:00"
    Then an exception of type "EntityAlreadyExistsException" should be thrown

  Scenario: User is not available in period due to other booking
    Given a company with the the following
      | Name | DTU |
    # Projects must have an initial project leader, giving this role to mads@kahler.biz is a quick workaround for this test
    And a user with the email "mads@kahler.biz"
    And a project with the following
      | Company       | DTU             |
      | Name          | Test Project    |
      | ProjectLeader | mads@kahler.biz |
    And the actor is a project leader for project "Test Project" for company "DTU"
    And the subject is a user
    And the subject has a booking to the project "Test Project" for company "DTU" from "10/05/2019 12:00" to "10/05/2019 16:00"
    When the actor books the subject to the project "Test Project" for company "DTU" from "10/05/2019 10:00" to "10/05/2019 16:00"
    Then an exception of type "EntityAlreadyExistsException" should be thrown