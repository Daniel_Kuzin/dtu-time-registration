package com.dema.timeregistration.picocontainer;

import com.dema.timeregistration.JpaRepositoryTestsConfiguration;
import org.picocontainer.injectors.Provider;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Provides a fresh EntityManager for each scenario
 */
public class EntityManagerProvider implements Provider {
    private EntityManagerFactory entityManagerFactory;

    @SuppressWarnings("unused")
    public EntityManager provide() {
        refreshEntityManagerFactory();
        return entityManagerFactory.createEntityManager();
    }

    private void refreshEntityManagerFactory() {
        if (entityManagerFactory != null) {
            entityManagerFactory.close();
        }

        entityManagerFactory = Persistence.createEntityManagerFactory("timeregistration", JpaRepositoryTestsConfiguration.properties);
    }
}
