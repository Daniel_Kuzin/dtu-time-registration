package com.dema.timeregistration;

import com.dema.timeregistration.entity.*;
import com.dema.timeregistration.repository.CompanyRepository;
import com.dema.timeregistration.repository.ProjectRepository;
import com.dema.timeregistration.repository.UserRepository;
import com.dema.timeregistration.service.UserContext;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

import java.text.ParseException;
import java.time.Instant;

public class TestUserProvider {
    private final UserRepository userRepository;
    private final CompanyRepository companyRepository;
    private final ProjectRepository projectRepository;

    private int currentId = 1;
    private UserContext actor;
    private User subject;

    public TestUserProvider(UserRepository userRepository, CompanyRepository companyRepository, ProjectRepository projectRepository) {
        this.userRepository = userRepository;
        this.companyRepository = companyRepository;
        this.projectRepository = projectRepository;
    }

    private User getNewUser() {
        String name = "test" + currentId++;
        return User.create(name + "@example.com", name, GlobalRole.GLOBAL_USER);
    }

    public User getSubject() {
        if (subject == null) {
            throw new IllegalStateException("Requested an subject but none was asserted");
        }

        return subject;
    }

    public UserContext getActor() {
        if (actor == null) {
            throw new IllegalStateException("Requested an actor but none was asserted");
        }

        return actor;
    }

    @When("^the subject is a user$")
    public void subjectIsUser() {
        subject = getNewUser();
        userRepository.save(subject);
    }

    @When("^the actor is an administrator$")
    public void actorIsAdmin() {
        userRepository.findByEmail("admin@example.com").ifPresent(userRepository::delete);
        User admin = User.create("admin@example.com", "Administrator", GlobalRole.GLOBAL_ADMINISTRATOR);
        userRepository.save(admin);
        actor = new UserContext(Instant.now(), admin);
    }

    @Given("the actor is a user")
    public void actorIsUser() {
        User user = getNewUser();
        userRepository.save(user);
        actor = new UserContext(Instant.now(), user);
    }

    @Given("a user with the email {string}")
    public void givenAUser(String email) {
        if (!userRepository.findByEmail(email).isPresent()) {
            userRepository.save(User.create(email, email, GlobalRole.GLOBAL_USER));
        }
    }

    @And("the subject is a user with role {string} for project {string} for company {string}")
    public void subjectWithProjectRole(String roleString, String project, String company) {
        Company foundCompany = companyRepository.findByName(company).orElseThrow(IllegalStateException::new);
        Project foundProject = projectRepository.findByCompanyAndName(foundCompany, project).orElseThrow(IllegalStateException::new);

        subject = getNewUser();

        subject.getProjectRoles().add(ProjectRole.create(Role.valueOf(roleString), foundProject, subject));
        userRepository.save(subject);
    }

    @Given("the subject is the actor")
    public void subjectIsActor() {
        subject = actor.getUser();
    }

    @Given("the actor is a project leader for project {string} for company {string}")
    public void actorIsProjectLeader(String project, String company) {
        User user = getNewUser();
        Company foundCompany = companyRepository.findByName(company).orElseThrow(IllegalStateException::new);
        Project foundProject = projectRepository.findByCompanyAndName(foundCompany, project).orElseThrow(IllegalStateException::new);

        user.getProjectRoles().add(ProjectRole.create(Role.PROJECT_LEADER, foundProject, user));
        userRepository.save(user);
        actor = new UserContext(Instant.now(), user);
    }

    @Given("the actor is a user with a booking for project {string} for company {string} between the dates {string} and {string}")
    public void actorHasProjectBooking(String project, String company, String startTime, String endTime) throws ParseException {
        Company foundCompany = companyRepository.findByName(company).orElseThrow(IllegalStateException::new);
        Project foundProject = projectRepository.findByCompanyAndName(foundCompany, project).orElseThrow(IllegalStateException::new);

        User user = getNewUser();

        Instant startTimeInstant = Utils.getDateFormat().parse(startTime).toInstant();
        Instant endTimeInstant = Utils.getDateFormat().parse(endTime).toInstant();

        user.getUserProjectBookings().add(
                UserProjectBooking.create(user, foundProject, startTimeInstant, endTimeInstant));

        userRepository.save(user);
        actor = new UserContext(Instant.now(), user);
    }
}
